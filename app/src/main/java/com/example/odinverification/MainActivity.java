package com.example.odinverification;

import androidx.appcompat.app.AppCompatActivity;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    MediaPlayer player;
    private static final int RECORDER_BPP = 16;
    private static final int RECORDER_SAMPLERATE = 8000;
    private static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_MONO;
    private static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;
    private int bufferSize = 0;
    private AudioRecord recorder = null;
    short[] buffer;
    private Thread recordingThread = null;
    private boolean isRecording = false;

    Switch swTrans, swRecv;
    Button btnStartTrans, btnStopTrans, btnStartRecv, btnStopRecv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);
        Anhxa();
        setButtonHandlers();
        enableButtons(false);
        bufferSize = AudioRecord.getMinBufferSize(RECORDER_SAMPLERATE,
                RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING);
    }

    private void Anhxa() {
        swTrans = (Switch)findViewById(R.id.swTrans);
        swRecv = (Switch)findViewById(R.id.swRecv);
        btnStartTrans = (Button)findViewById(R.id.btnStartTrans);
        btnStopTrans = (Button)findViewById(R.id.btnStopTrans);
        btnStartRecv = (Button)findViewById(R.id.btnStartRecv);
        btnStopRecv = (Button)findViewById(R.id.btnStopRecv);
        btnStartTrans.setEnabled(true);
        btnStopTrans.setEnabled(true);
        btnStartRecv.setEnabled(true);
        btnStopRecv.setEnabled(true);
    }

    public void startTrans(View v) {
        if(player == null) {
            player = MediaPlayer.create(this, R.raw.song);
            player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    stopPlayer();
                }
            });
        }
        player.start();
    }

    public void stopTrans(View v) {
        stopPlayer();
    }

    private void stopPlayer(){
        if(player != null)
            player.release();
            player = null;
            Toast.makeText(this, "MediaPlayer released", Toast.LENGTH_SHORT).show();
    }

    private void setButtonHandlers() {
        btnStartRecv.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                
            }
        });
        btnStopRecv.setOnClickListener();
    }
    private void enableButton(int id, boolean isEnable) {
        ((Button) findViewById(id)).setEnabled(isEnable);
    }
    private void enableButtons(boolean isRecording) {
        enableButton(R.id.btnStartRecv, !isRecording);
        enableButton(R.id.btnStopRecv, isRecording);
    }
    private void writeAudioDataToByte() {

        // Write the output audio in byte
        byte data[] = new byte[bufferSize];
        while (isRecording) {
            recorder.read(buffer, 0, buffer.length);
        }
        for (int i = 0; i < data.length; i++) {
            System.out.println(data[i]);
        }
    }

    public void StartRecv(View v) {
        int buffercount = 4088 / bufferSize;
        if (buffercount < 1)
            buffercount = 1;

        recorder = new AudioRecord(MediaRecorder.AudioSource.MIC,
                RECORDER_SAMPLERATE, RECORDER_CHANNELS,
                RECORDER_AUDIO_ENCODING, bufferSize * buffercount);

        buffer = new short[4088];

        recorder.startRecording();


        recordingThread = new Thread(new Runnable() {

            @Override
            public void run() {

                writeAudioDataToByte();

            }
        }, "AudioRecorder Thread");

        recordingThread.start();
    }

    public void StopRecv(View v) {

    }
}